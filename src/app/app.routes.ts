import { Route } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ImpressumComponent } from './impressum/impressum.component';

export const appRoutes: Route[] = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'impressum',
    component: ImpressumComponent,
    pathMatch: 'full'
  },
  {
    path: 'news',
    loadComponent: () => import('@oros/news').then((n) => n.NewsComponent)
  },
  {
    path: 'blog',
    loadComponent: () => import('@oros/blog').then((n) => n.BlogComponent)
  }
];
