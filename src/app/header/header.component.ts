import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { ThemeServiceService } from '../services/theme-service.service';
import { Router } from '@angular/router';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'oros-header',
  standalone: true,
  imports: [CommonModule, MenubarModule, ButtonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  light = false;

  constructor(private themeService: ThemeServiceService, private router: Router) {
  }

  navigateTo(route: string) {
    this.router.navigate(['/' + route]);
  }

  toggleTheme() {
    this.light = !this.light;
    let themeName = this.light ? 'lara-light-teal' : 'lara-dark-teal';
    this.themeService.setTheme(themeName);
  }


  menuItems: MenuItem[] = [
    {
      label: 'Home',
      icon: 'pi pi-fw pi-home',
      routerLink: '/',
      styleClass: 'font-bold'
    }, {
      label: 'News',
      icon: 'pi pi-fw pi-megaphone',
      routerLink: 'news',
      styleClass: 'font-bold'
    },
    {
      label: 'Blog',
      icon: 'pi pi-fw pi-book',
      routerLink: 'blog',
      styleClass: 'font-bold'
    }
  ];
}
