import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { FieldsetModule } from 'primeng/fieldset';
import { PanelModule } from 'primeng/panel';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollTopModule } from 'primeng/scrolltop';

@Component({
  selector: 'oros-home',
  standalone: true,
  imports: [CommonModule, ButtonModule, CardModule, ScrollTopModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent {
  isDisabled  = false;

  openInNewWindow(url: string) {
    window.open(url, '_blank');
  }
}
