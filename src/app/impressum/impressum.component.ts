import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'oros-impressum',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './impressum.component.html',
  styleUrl: './impressum.component.css',
})
export class ImpressumComponent {}
